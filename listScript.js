
import gettingCard from "./gettingCard.js";
import createList from "./createList.js";
import popUpCard from "./cardPopUp.js";
import checkListPopUp from "./checkListPopUp.js";
import checkListItem from "./checkListItem.js";
import getCheckListData from "./getChekListData.js";

const cardContainer = document.getElementById("cardContainer");

let key = "4cddeae38cdcd97ce3d2471f4d8a3699";
let token = "7d3f07d3c2f4f4ffa8dd47f602b71396f14f2e947b32347b719c1835ac98bc9b";

const urlPath = window.location.href;
const newUrlPath = new URL(urlPath);
const boardId = newUrlPath.searchParams.get("id");
const boardName = newUrlPath.searchParams.get("name");

window.addEventListener('load', async function () {


    let list = await fetch(`https://api.trello.com/1/boards/${boardId}/lists?key=${key}&token=${token}`, {
        method: 'GET'
    })

    let listdata = await list.json();
    createList(listdata[0])

    // Create card 
    document.getElementById("listButton").addEventListener('click', (event) => {
        let NameOfCard = document.getElementById("textId").value;

        fetch(`https://api.trello.com/1/cards?key=${key}&token=${token}&idList=${listdata[0].id}&name=${NameOfCard}`, {
            method: 'POST'
        }).then(data => data.json())
            .then(data => gettingCard(data))

    })

    let card = await fetch(`https://api.trello.com/1/lists/${listdata[0].id}/cards?key=${key}&token=${token}`, {
        method: 'GET'
    })
    let carddata = await card.json();

    if (carddata.length) {
        carddata.forEach(data => {
            gettingCard(data)
        })
    }


 

    carddata.forEach(item => {

        let cardInfo = document.getElementById(item.id);
        cardInfo.addEventListener('click', (event) => {

            popUpCard(event.target);

            let checkListButtonInfo = document.querySelector("#checkListButton");
            checkListButtonInfo.addEventListener('click', (event) => {
                checkListPopUp();
            })

            addCheckListTitle(event.target.id);
        })

        // geting checklist
        item.idChecklists.forEach(checklistId => {
            getCheckListData(checklistId, key, token);

        })
    })

    function addCheckListTitle(cardId) {
        // console.log(cardId);
        let addButton = document.getElementById('addCheckList');
        addButton.addEventListener('click', (event) => {
            let title = document.getElementById('checkListInput').value;
            console.log(title);
            // checkListItemAdded()
            let checkData = fetch(`https://api.trello.com/1/checklists?key=${key}&token=${token}&idCard=${cardId}&name=${title}`, {
                method: 'POST'

            }).then(res => res.json())
                .then((data) => {
                    // console.log("data", data);
                    checkListItem(data)
                })
        })


    }

    setTimeout(timeFunction, 1000);

    function timeFunction() {
       
        let addItemButton = document.querySelectorAll('.addCheckListItemButton');
        console.log("button click", addItemButton);
   
       for (let i = 0; i < addItemButton.length; i++) {
   
           addItemButton[i].addEventListener('click', (event) => {
   
               let itemValue = document.getElementById(addItemButton[i].id + "1").value;
   
               console.log(addItemButton[i].id);
               fetch(`https://api.trello.com/1/checklists/${addItemButton[i].id}/checkItems?key=${key}&token=${token}&name=${itemValue}`, {
                   method: 'POST'
               })
                   .then(response => {
                       console.log(
                           `Response: ${response.status} ${response.statusText}`
                       );
                       return response.json();
                   })
                   .then(text => {
                       console.log("text", text)
                       createAddItem2(text);
   
                   })
                   .catch(err => console.error(err));
   
           })
   
       }

    }
})



const createAddItem2 = (item) => {
    let container = document.getElementById(item.idChecklist);
    let inputValue = document.getElementById(item.idChecklist + "1");
    let itemSpan = document.createElement('span');
    itemSpan.setAttribute('class', "itemAddSpan");
    itemSpan.setAttribute('id', item.name);
    itemSpan.innerText = item.name;
    container.insertBefore(itemSpan, inputValue);
}

