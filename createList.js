
let createList = (data) => {
   
    let listDiv = document.createElement('div');
    let childDiv1 = document.createElement('div')
    let spanCard = document.createElement('span');
    let button = document.createElement('button');
    let inputText = document.createElement('input');
    childDiv1.setAttribute('class', "buttonDiv")
    button.setAttribute('id', 'listButton')
    button.setAttribute("class",'btn btn-primary')
    button.innerText = "Add card";
    inputText.setAttribute('type', 'text');
    inputText.setAttribute('id', "textId");
    listDiv.setAttribute('id', data.id);
    listDiv.setAttribute('class', "cardList");
    spanCard.setAttribute('class', "spanCard");
    spanCard.innerText = data.name;
     listDiv.style.height = "min-content";
    
     listDiv.style.width = "207px";
    listDiv.style.background = "white";
    listDiv.style.borderRadius = "10px"
    spanCard.style.fontSize = "15px";
    childDiv1.appendChild(inputText);
    listDiv.appendChild(spanCard);
    childDiv1.appendChild(button);
    listDiv.appendChild(childDiv1);
    cardContainer.appendChild(listDiv);

}

export default createList;