
let fetchCard = (id, key, token) => {
     
    return fetch(`https://api.trello.com/1/boards/${id}/lists?key=${key}&token=${token}`, {
        method: 'GET'
    }).then((res) => {
        return res.json();
    }).then(data => {
        // console.log(data);
        return data;
    })

}

export default fetchCard;