
import data from "./boards.js";
import boards from "./createBoard.js";
// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function () {
  modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function () {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}

let cardButtton = document.getElementById('btn');
cardButtton.addEventListener('click', myFunction)


function myFunction() {
  var v = document.getElementById("myText").value;
  boards(v).then(response => {
    console.log(
      `Response: ${response.status} ${response.statusText}`
    );
    return response.text();
  })
    .then(text => console.log(text))
    .catch(err => console.error(err));
  document.getElementById("demo").innerHTML = v;
}

function createBoard(boardId, boardName) {
                  let newLink = document.createElement('a');
                  newLink.setAttribute('id', "boards-link");
                  newLink.setAttribute('href', `lists.html?id=${boardId}&name=${boardName}`);
                  
                  let newBoard = document.createElement('div');
                  newBoard.setAttribute('id', 'boards') //:TODO: Change the id

                  let spanBoards = document.createElement('span');
                  spanBoards.innerText = boardName;

                  newLink.appendChild(newBoard);
                newBoard.appendChild(spanBoards);
 let boardBody = document.getElementById('board-body')
 boardBody.append(newLink);   
}


(async ()=>{
  const data1 = await data();
  data1.forEach(item => {
     createBoard(item.id, item.name);
  })
})();

