import checkListItem from "./checkListItem.js";
//  import checkItemAdd from "./checkItemAdd.js";
// import createAddItem from "./checkListAddItemCreate.js";
let getCheckListData = (checkId, key, token) => {
    fetch(`https://api.trello.com/1/checklists/${checkId}?key=${key}&token=${token}`, {
        method: 'GET'
    }).then(res => res.json())
    .then(data => {
        checkListItem(data)
    })
}

export default getCheckListData;