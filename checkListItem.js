// let key = "4cddeae38cdcd97ce3d2471f4d8a3699";
// let token = "7d3f07d3c2f4f4ffa8dd47f602b71396f14f2e947b32347b719c1835ac98bc9b";


import createAddItem from "./checkListAddItemCreate.js";


let checkListItem = (dataItem) => {

    let container = document.getElementById('checkContainer');
    let checklistDiv = document.createElement('div');
    let checklistSpan = document.createElement('span');
    let checklistInput = document.createElement('input');
    let chekListIdButton = document.createElement('button');
    checklistSpan.innerText = dataItem.name;
    chekListIdButton.innerText = "Add an item";
    checklistDiv.setAttribute('class', "checkListGet");
    checklistDiv.setAttribute('id', dataItem.id);
    checklistSpan.setAttribute('class', "checkSpan");
    checklistInput.setAttribute('type', "text");
    checklistInput.setAttribute("class", "addItemCheckList");
    checklistInput.setAttribute("id", dataItem.id + "1");
    chekListIdButton.setAttribute('type', "button");
    chekListIdButton.setAttribute('class', "addCheckListItemButton")
    chekListIdButton.setAttribute('id', dataItem.id);
    checklistDiv.appendChild(checklistSpan);
    checklistDiv.appendChild(checklistInput);
    checklistDiv.appendChild(chekListIdButton);
    container.appendChild(checklistDiv);

    createAddItem(dataItem.checkItems);
 

}
export default checkListItem;