
let removeCard = (e, key, token) => {
  fetch(`https://api.trello.com/1/cards/${e.id}?key=${key}&token=${token}`, {
    method: 'DELETE'
  })
    .then(response => {
      console.log(
        `Response: ${response.status} ${response.statusText}`
      );
      return response.text();
    })
    .then(() => e.parentElement.remove())
    .catch(err => console.error(err));
}

export default removeCard;