  import createAddItem from "./checkListAddItemCreate.js";

let key = "4cddeae38cdcd97ce3d2471f4d8a3699";
let token = "7d3f07d3c2f4f4ffa8dd47f602b71396f14f2e947b32347b719c1835ac98bc9b";

let checkItemAdd = (data) => {
    fetch(`https://api.trello.com/1/checklists/${data.id}/checkItems?key=${key}&token=${token}`, {
        method: 'GET'
    })
        .then(response => {
            console.log(
                `Response: ${response.status} ${response.statusText}`
            );
            return response.json();
        })
        .then(text => {
            text.forEach(item => {
                createAddItem(item);
            })
             
            console.log(text)
        })
        .catch(err => console.error(err));

}

export default checkItemAdd;